// const webpack = require('webpack')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

module.exports = {
  pwa: {
    name: 'Downbeat',
    themeColor: '#B8E986',
    msTileColor: '#4A4A4A'
    // BACKLOG: If the load process is done in fullscreen mode (navigator.standalone==true), always load the home screen no matter what the stored URL is
    // Reference: https://medium.com/@firt/dont-use-ios-web-app-meta-tag-irresponsibly-in-your-progressive-web-apps-85d70f4438cb
    // appleMobileWebAppCapable: 'yes',
    // appleMobileWebAppStatusBarStyle: 'black'
  },
  configureWebpack: {
    // output: {
    //   globalObject: 'this' // `typeof self !== 'undefined' ? self : this`'' -- not working
    // },
    // devServer: {
    //   hot: true
    // },
    plugins: [
      // new webpack.HotModuleReplacementPlugin()
      new BundleAnalyzerPlugin()
    ]
  }
}
