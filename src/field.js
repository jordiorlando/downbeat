import Fraction from './fraction.js'

/* const FIELD = {
  width: 360,
  height: 160,
  border: 6,
  line: {
    width: new Fraction(0, 1, 3),
    length: 2
  },
  numbers: {
    top: {
      highschool: 27,
      college: 27,
      pro: 42
    },
    width: 4,
    height: 6
  },
  hashes: {
    front: {
      highschool: new Fraction(53, 1, 3),
      college: 60,
      pro: new Fraction(70, 3, 4)
    },
    back: {
      highschool: new Fraction(106, 2, 3),
      college: 100,
      pro: new Fraction(89, 1, 4)
    }
  },
  tee: {
    highschool: 40,
    college: 35,
    pro: 35
  }
} */

const ft = f => Array.isArray(f) ? (new Fraction(f)).multiply(12) : f * 12
const yd = y => Array.isArray(y) ? (new Fraction(y)).multiply(3 * 12) : y * 3 * 12
// const st = s => Array.isArray(s) ? (new Fraction(s)).multiply([22, 1, 2]) : s * 22.5

const styles = {
  default: {
    light: {
      default: {
        fill: 0xFFFFFF,
        stroke: 0x212121,
        strokeWidth: 4
      },
      border: {
        fill: 0xFFFFFF,
        stroke: 0xFFFFFF,
        strokeWidth: 0
      }
    },
    dark: {
      default: {
        fill: 0x212121,
        stroke: 0xFAFAFA,
        strokeWidth: 4
      },
      border: {
        fill: 0x212121,
        stroke: 0x212121,
        strokeWidth: 0
      }
    }
  }
}
/* const strokeWidth = FIELD.line.width.valueOf()
const lineColor = 0x212121 // colors.grey.darken4
const fillColor = 0xFFFFFF // colors.white */

const graphics = {
  turf: [
    {
      style: 'border',
      type: 'rectangle',
      x: yd(-62),
      y: ft(-86),
      w: yd(124),
      h: ft(172)
    },
    {
      style: 'turf',
      type: 'rectangle',
      x: yd(-60),
      y: ft(-80),
      w: yd(120),
      h: ft(160)
    }
  ],
  endzone: [
    {
      style: 'endzone',
      type: 'rectangle',
      x: yd(-5),
      y: ft(-80),
      w: yd(10),
      h: ft(160)
    }
  ],
  yardmark: [
    {
      style: 'mark',
      type: 'line',
      x1: 0,
      y1: ft(-1),
      x2: 0,
      y2: ft(1)
    }
  ],
  teemark: [
    {
      style: 'mark',
      type: 'line',
      x1: ft(-1),
      y1: ft(-1),
      x2: ft(1),
      y2: ft(1)
    },
    {
      style: 'mark',
      type: 'line',
      x1: ft(1),
      y1: ft(-1),
      x2: ft(-1),
      y2: ft(1)
    }
  ],
  hashmark: [
    {
      style: 'mark',
      type: 'line',
      x1: ft(-1),
      y1: 0,
      x2: ft(1),
      y2: 0
    }
  ],
  yardline: [
    {
      style: 'yardline',
      type: 'line',
      x1: 0,
      y1: ft(-80),
      x2: 0,
      y2: ft(80)
    },
    {
      name: 'yardmark',
      type: 'graphic',
      graphic: 'yardmark',
      pos: [
        { x: yd(-2), y: ft([-78, 1, 2]) },
        { x: yd(-2), y: ft([78, 1, 2]) },
        { x: yd(-1), y: ft([-78, 1, 2]) },
        { x: yd(-1), y: ft([78, 1, 2]) },
        { x: yd(1), y: ft([-78, 1, 2]) },
        { x: yd(1), y: ft([78, 1, 2]) },
        { x: yd(2), y: ft([-78, 1, 2]) },
        { x: yd(2), y: ft([78, 1, 2]) }
      ]
    },
    {
      name: 'hashmark',
      type: 'graphic',
      graphic: 'hashmark',
      pos: [
        { x: 0, y: ft([-26, 2, 3]) }, // High School
        { x: 0, y: ft([26, 2, 3]) },
        { x: 0, y: ft(-20) }, // College
        { x: 0, y: ft(20) },
        { x: 0, y: ft([-9, 1, 4]) }, // Pro
        { x: 0, y: ft([9, 1, 4]) }
      ]
    }
  ],
  field: [
    {
      name: 'turf',
      type: 'graphic',
      graphic: 'turf',
      pos: [
        { x: 0, y: ft(-80) }
      ]
    },
    {
      name: 'endzone',
      type: 'graphic',
      graphic: 'endzone',
      pos: [
        { x: yd(-55), y: ft(-80) },
        { x: yd(55), y: ft(-80) }
      ]
    },
    {
      name: '3yardmark',
      type: 'graphic',
      graphic: 'yardmark',
      pos: [
        { x: yd(-47), y: ft(-80) },
        { x: yd(47), y: ft(-80) }
      ]
    },
    {
      name: 'teemark',
      type: 'graphic',
      graphic: 'teemark',
      pos: [
        { x: yd(-10), y: ft(-80) }, // High School (40 yard line)
        { x: yd(10), y: ft(-80) },
        { x: yd(-15), y: ft(-80) }, // College, Pro (35 yard line)
        { x: yd(15), y: ft(-80) }
      ]
    },
    {
      name: 'yardline',
      type: 'graphic',
      graphic: 'yardline',
      pos: [
        { x: yd(-45), y: ft(-80) }, // 5 yard line
        { x: yd(-40), y: ft(-80) }, // 10 yard line
        { x: yd(-35), y: ft(-80) }, // 15 yard line
        { x: yd(-30), y: ft(-80) }, // 20 yard line
        { x: yd(-25), y: ft(-80) }, // 25 yard line
        { x: yd(-20), y: ft(-80) }, // 30 yard line
        { x: yd(-15), y: ft(-80) }, // 35 yard line
        { x: yd(-10), y: ft(-80) }, // 40 yard line
        { x: yd(-5), y: ft(-80) }, // 45 yard line
        { x: yd(0), y: ft(-80) }, // 50 yard line
        { x: yd(5), y: ft(-80) }, // 45 yard line
        { x: yd(10), y: ft(-80) }, // 40 yard line
        { x: yd(15), y: ft(-80) }, // 35 yard line
        { x: yd(20), y: ft(-80) }, // 30 yard line
        { x: yd(25), y: ft(-80) }, // 25 yard line
        { x: yd(30), y: ft(-80) }, // 20 yard line
        { x: yd(35), y: ft(-80) }, // 15 yard line
        { x: yd(40), y: ft(-80) }, // 10 yard line
        { x: yd(45), y: ft(-80) } // 5 yard line
      ]
    }/* ,
    {
      // TODO: Goal lines
      name: 'goalline',
      type: 'graphic',
      graphic: 'goalline',
      pos: [
        { x: -150, y: -80 },
        { x: 150, y: -80 }
      ]
    } */
  ],
  performer: []
}

export { Fraction, ft, yd, styles, graphics }
