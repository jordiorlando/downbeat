import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'
import createPersistedState from 'vuex-persistedstate'

import state from './state'
import getters from './getters'
import mutations from './mutations'
import actions from './actions'
import modules from './modules'

Vue.use(Vuex)

const store = new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
  modules,
  plugins: process.env.NODE_ENV !== 'production' ? [
    createLogger(),
    createPersistedState()
  ] : [
    createPersistedState()
  ],
  strict: process.env.NODE_ENV !== 'production'
})

if (module.hot) {
  // accept actions and mutations as hot modules
  module.hot.accept([
    './state',
    './getters',
    './mutations',
    './actions',
    './modules'
  ], () => {
    // require the updated modules
    // have to add .default here due to babel 6 module output
    // swap in the new actions and mutations
    store.hotUpdate({
      state: require('./state'), // .default
      getters: require('./getters'),
      mutations: require('./mutations'),
      actions: require('./actions'),
      modules: require('./modules')
    })
  })
}

export default store
