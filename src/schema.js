export default {
  '0.0.3': {
    props: {
      name: {
        type: 'string'
      },
      previous: {
        type: 'string'
      },
      next: {
        type: 'string'
      },
      sets: {
        type: 'array',
        element: {
          name: 'set',
          type: 'object',
          props: {
            name: {
              alias: 'n',
              type: 'string'
            },
            counts: {
              alias: 'c',
              type: 'number'
            }
          }
        }
      },
      performers: {
        type: 'array',
        element: {
          name: 'performer',
          type: 'object',
          props: {
            name: {
              alias: 'n',
              type: 'string',
              format: '$type$squad$position$number',
              props: {
                type: {
                  type: 'char'
                },
                squad: {
                  type: 'number'
                },
                position: {
                  alias: 'pos',
                  type: 'char'
                },
                number: {
                  alias: 'num',
                  type: 'number'
                }
              }
            },
            sets: {
              alias: 's',
              type: 'array',
              element: {
                name: 'set',
                type: 'array',
                element: {
                  name: 'subset',
                  type: 'object',
                  props: {
                    type: {
                      alias: 't',
                      type: 'string',
                      required: true
                    },
                    counts: {
                      alias: 'c',
                      type: 'number',
                      required: true
                    },
                    x: {
                      type: 'fraction',
                      required: true
                    },
                    y: {
                      type: 'fraction',
                      required: true
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
